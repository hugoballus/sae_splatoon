"""Module de gestion des joueurs
"""
import const


def Joueur(couleur, nom, reserve, surface, position, objet, duree_objet):
    """Créer un nouveau joueur à partir de ses caractéristiques

    Args:
        couleur (str): une lettre majuscule indiquant la couleur du joueur
        nom (str): un nom de joueur
        reserve (int): un entier qui indique la réserve de peinture du joueur
        surface (int): un entier inquant le nombre de cases peintes par le joueur
        position (tuple): une pair d'entier indiquant sur quelle case se trouve le joueur
        objet (int): un entier indiquant l'objet posédé par le joueur (case.AUCUN si pas d'objet)
        duree_objet (int): un entier qui indique pour combier de temps le joueur a l'objet

    Returns:
        dict: un dictionnaire représentant le joueur
    """
    joueur = {"couleur": couleur, "nom": nom, "reserve": reserve, "surface": surface, "position": position, "objet": objet, "duree_objet": duree_objet}
    return joueur
    
def joueur_from_str(description):
    """créer un joueur à partir d'un chaine de caractères qui contient
        ses caractéristiques séparées par des ; dans l'ordre suivant:
        "couleur;reserve;surface;objet;duree_objet;lin;col;nom_joueur"

    Args:
        description (str): la chaine de caractères contenant les caractéristiques
                            du joueur

    Returns:
        dict: le joueur ayant les caractéristiques décrite dans la chaine.
    """
    description_champ = description.split(";")
    A=Joueur(description_champ[0],description_champ[7],int(description_champ[1]),int(description_champ[2]),(int(description_champ[5]),int(description_champ[6])),int(description_champ[3]),int(description_champ[4]))
    return A

def get_couleur(joueur):
    """retourne la couleur du joueur

    Args:
        joueur (dict): le joueur considéré

    Returns:
        str: une lettre indiquant la couleur du joueur
    """
    return joueur["couleur"]


def get_nom(joueur):
    """retourne le nom du joueur

    Args:
        joueur (dict): le joueur considéré

    Returns:
        str: le nom du joueur
    """
    return joueur["nom"]


def get_reserve(joueur):
    """retourne la valeur de la réserve du joueur
    joueur (dict): le joueur considéré

    Returns:
        int: la réserve du joueur
    """
    return joueur["reserve"]


def get_surface(joueur):
    """retourne le nombre de cases peintes par le joueur
        Attention on ne calcule pas ce nombre on retourne juste la valeur
        stockée dans le joueur
    joueur (dict): le joueur considéré
    if objet == const.BIDON and joueur["reserve"] > 0 :
        joueur["objet"] = objet
        joueur["duree"] = duree
    Returns:
        int: le nombre de cases peintes du joueur
    """
    return joueur["surface"]


def get_objet(joueur):
    """retourne l'objet possédé par le joueur (case.AUCUN pour aucun objet)
    joueur (dict): le joueur considéré

    Returns:
        int: un entier indiquant l'objet possédé par le joueur
    """
    return joueur["objet"]
    

def get_duree(joueur):
    """retourne la duree de vie de l'objet possédé par le joueur
    joueur (dict): le joueur considéré

    Returns:
        int: un entier indiquant la durée de vie l'objet possédé par le joueur
    """
    return joueur["duree_objet"]

    
def get_pos(joueur):
    """retourne la position du joueur. ATTENTION c'est la position stockée dans le
        joueur. On ne la calcule pas
    joueur (dict): le joueur considéré

    Returns:
        tuple: une paire d'entiers indiquant la position du joueur.
    """
    return joueur["position"]


def set_pos(joueur, pos):
    """met à jour la position du joueur

    Args:
        joueur (dict): le joueur considéré
        pos (tuple): une paire d'entier (lin,col) indiquant la position du joueur
    """
    joueur["position"] = pos
    return joueur


def modifie_reserve(joueur, quantite):
    """ modifie la réserve du joueur.
        ATTENTION! La quantité peut être négative et le réserve peut devenir négative

    Args:
        joueur (dict): le joueur considéré
        quantite (int)): un entier positif ou négatif inquant la variation de la réserve

    Returns:
        int: la nouvelle valeur de la réserve
    """
    joueur["reserve"] += quantite
    return joueur["reserve"] 

def set_surface(joueur, surface):
    """met à jour la surface du joueur

    Args:
        joueur (dict): le joueur considéré
        surface (int): la nouvelle valeur de la surface
    """
    joueur["surface"] = surface
    return joueur

def ajouter_objet(joueur, objet, duree):
    """ajoute un objet au joueur (celui-ci ne peut en avoir qu'un à la fois).
        Si l'objet est const.BIDON on change pas l'objet mais on remet à 0 la
        réserve du joueur si celle ci est négative
    Args:
        joueur (dict): le joueur considéré
        objet (int): l'objet considéré
        duree (int): la durée de vie de l'objet
    """
    if objet == const.BIDON and get_reserve(joueur) < 0:
        joueur["reserve"] = 0
    elif objet == const.BIDON:
        return joueur
    else:
        joueur["objet"] = objet
        joueur["duree_objet"] = duree
    return joueur
    
ajouter_objet(joueur_from_str("A;-3;28;2;10;0;1;nom_joueur"), 3, 10)
    
def maj_duree(joueur):
    """décrémente la durée de vie de l'objet du joueur (si celui-ci en a un).
        Si la durée arrive à 0 l'objet disparait

    Args:
        joueur (dict): le joueur considéré
    """
    if joueur["objet"] != const.AUCUN :
        joueur["duree_objet"] -= 1
    if joueur["duree_objet"] == 0 :
        joueur["objet"] = const.AUCUN
    return joueur
    

